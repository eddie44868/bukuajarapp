import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class Pengantar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Kata Pengantar",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/contract.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Puji syukur kami panjatkan kehadirat Allah SWT berkat limpahan rahmat dan kasih sayangnya-Nyam kita semua masih diberi kesehatan walaupun masih dalam pandemi Covid-19. Kita semua tahu bahwa pandemi yang sudah berlangsung hampir 2 tahun ini membuat sistem pembelajaran di seluruh sekolah menjadi daring. Tentu hal ini menjadi tantangan tersendiri karena kita semua dipaksa untuk menggunakan berbagai macam teknologi digital sehingga kegiatan belajar mengajar masih tetap dapat berlangsung.'
                    '\n\nBuku ajar digital ini merupakan luaran dari Penelitian Kebijakan Mahasiswa FMIPA Unesa 2021 sehingga penulis mengucapkan terima kasih banyak kepada pihak-pihak yang telah membantu dalam proses penyusunannya. Dalam buku ini memuat materi Medan Magnet yang merupakan salah satu materi mikroskopis sehingga lebih sulit dipahami dibandingkan materi fisika yang makroskopis. Oleh karena itu, dalam buku ini dilengkapi dengan 3D Model yang interaktif sehingga meningkatkan rasa ingin tahu dan pemahaman peserta didik dalam mempelajari materi ini. Tidak hanya itu, buku ini disusun dengan pelaksanaan pembelajaran Problem-Based Learning yang dapat meningkatkan keterampilan pe- mecahan masalah siswa. ',
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: StyledText(
                    text:
                        'Penulis menyadari bahwa masih banyak kekurangan yang terdapat dalam buku ini. Kritik, saran, masukan, dan evaluasi sangat penting untuk memperbaiki dan mengembangkan buku ini sehingga dapat lebih baik lagi di masa mendatang. Kedepannya, buku ini akan dipublikasi melalui <italic>Google Play Store</italic> dan dapat diunduh secara gratis oleh seluruh pendidik di Indonesia. Semoga buku digital ini dapat bermanfaat bagi kita semua.',
                    tags: {
                      'bold': StyledTextTag(
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      'italic': StyledTextTag(
                          style: TextStyle(fontStyle: FontStyle.italic)),
                    },
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Image.asset('assets/kata.png'),
                ),
                StyledText(
                    text:
                        '<italic>Cheers :)</italic>',
                    tags: {
                      'bold': StyledTextTag(
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      'italic': StyledTextTag(
                          style: TextStyle(fontStyle: FontStyle.italic)),
                    },
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.justify,
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
