import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class Dapus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Daftar Pustaka",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/books.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: StyledText(
                    text:
                        'Halliday, D., Resnick, R., dan Walker, J. 2014. <italic>Fundamentals of Physics, Tenth Ed.</italic> New Jersey: John Wiley & Sons, Inc'
                        '\n\nHandayani, S. dan Damari, A. 2009. <italic>Fisika untuk SMA dan MA kelas XII.</italic> Jakarta: Pusat Perbukuan Departemen Pendidikan Nasional'
                        '\n\nSerway, R.A. dan Jewett, J.W. 2014. <italic>Physics for Scientist and Engineers with Modern Physics, Ninth Ed.</italic> Boston: Brooks/Cole'
                        '\n\nSetyawati, A. dkk. 2018. <italic>Buku Pintar Belajar Fisika untuk SMA/MA Kelas XII.</italic> Sagufindo Kinarya'
                        '\n\nSuharyanto, S., Karyono, K, dan Palupi, D.S. 2009. <italic>Fisika untuk SMA dan MA kelas XII.</italic> Jakarta: Pusat Perbukuan Departemen Pendidikan Nasional'
                        '\n\nSuparmo, S. dan Widodo, T. 2009. <italic>Panduan Pembelajaran Fisika XII.</italic> Jakarta: Pusat Perbukuan Departemen Pendidikan Nasional',
                    tags: {
                      'bold': StyledTextTag(
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      'italic': StyledTextTag(
                          style: TextStyle(fontStyle: FontStyle.italic)),
                    },
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
