import 'package:buku_ajar_app/materiPage/bab3/drawer3.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class bab3_lkpd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen3(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 50),
            Text(
              "Gaya Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet2.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz35.png',
                    height: 400,
                    width: double.infinity,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
