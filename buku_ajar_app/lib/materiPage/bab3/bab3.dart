import 'package:buku_ajar_app/materiPage/bab3/drawer3.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class bab3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen3(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 50),
            Text(
              "Gaya Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet2.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Text(
                  'Muatan-muatan elektron yang bergerak akan menimbulkan arus listrik. Oleh karena elektron yang bergerak di dalam medan magnet mengalami gaya magnet, maka jelas bahwa sebuah kawat berarus yang diletakkan di dalam medan magnetik akan mengalami gaya magnetik.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 30, bottom: 10),
                  child: Text(
                    '\n3.1 Gaya Magnetik pada Penghantar Berarus '
                    '\n      dalam Medan Magnet',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Perhatikan Gambar 11 di samping. Sebuah kawat penghantar AB yang dibentangkan melalui medan magnet yang ditimbulkan oleh magnet tetap. Apabila pada ujung kawat A kita hubungkan dengan kutub positif baterai dan ujung B kita hubungkan dengan kutub negatif baterai, maka pada kawat AB mengalir arus dari A ke B. '
                      '\n\nPada saat itu kawat AB akan bergerak ke atas. Sebaliknya jika arus listrik diputus (dihentikan), kawat kembali ke posisi semula. Sebaliknya jika ujung A dihubungkan dengan kutub negatif dan ujung B dihubungkan dengan kutub positif baterai, kembali kawat bergerak ke bawah (berlawanan dengan gerak semula).'
                      '\n\nGerakan kawat ini menunjukkan adanya suatu gaya yang bekerja pada kawat tersebut saat kawat tersebut dialiri arus listrik. <italic>Gaya yang bekerja pada tersebut disebut gaya magnetik atau gaya Lorentz.</italic>',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Image.asset(
                    'assets/gambar11.png',
                    height: 200,
                    width: 300,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 11. Gaya Lorentz pada kawat berarus dalam medan magnetik.',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                StyledText(
                  text:
                      '\nBerdasarkan hasil percobaan yang lebih teliti menunjukkan bahwa besarnya gaya magnetik gaya Lorentz yang dialami oleh kawat yang beraliran arus listrik : '
                      '\n\n<bold>a.</bold>	Berbanding lurus dengan kuat medan magnet/induksi magnet (B).'
                      '\n<bold>b.</bold>	Berbanding lurus dengan kuat arus listrik yang mengalir dalam kawat (I).'
                      '\n<bold>c.</bold>	Berbanding lurus dengan panjang kawat penghantar ( l ).'
                      '\n<bold>d.</bold>	Berbanding lurus dengan sudut (T) yang dibentuk arah arus (I) dengan arah induksi magnet (B).',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nBesarnya gaya magnetik/gaya Lorentz dapat dinyatakan dalam persamaan matematis :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus11.png',
                  height: 70,
                  width: 200,
                ),
                SizedBox(height: 5),
                Image.asset(
                  'assets/dgn3.png',
                  height: 70,
                  width: double.infinity,
                ),
                Text(
                  '\nKarena gaya merupakan besaran vektor, maka kita juga harus mengetahui arah gaya Lorentz yang dihasilkan. Untuk mengetahuinya, kita bisa menggunakan kaidah tangan kanan seperti pada Gambar 12 dengan ketentuan sebagai berikut.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/catatan3.png',
                  height: 120,
                  width: double.infinity,
                ),
                Image.asset(
                  'assets/tangan kanan buka i.gif',
                  height: 250,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 12. Kaidah tangan kanan untuk menentukan arah gaya Lorentz, medan magnet, dan kuat arus.',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
