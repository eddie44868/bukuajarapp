import 'package:buku_ajar_app/materiPage/bab3/drawer3.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class bab3_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen3(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 50),
            Text(
              "Gaya Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet2.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only(right: 10, bottom: 10),
                  child: Text(
                    '3.2 Gaya Magnetik di Antara Dua Kawat Sejajar '
                    '\n       Berarus',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Gaya magnet juga dialami oleh dua buah kawat sejajar yang saling berdekatan yang beraliran arus listrik. Timbulnya gaya pada masing-masing kawat dapat dianggap bahwa kawat pertama berada dalam medan magnetik yang ditimbulkan oleh kawat kedua dan juga sebaliknya kawat kedua berada dalam medan magnetik yang ditimbulkan oleh kawat pertama.'
                      '\n\nArah gaya magnetik yang terjadi pada kedua kawat dapat dilihat pada Gambar 13, dimana :',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: StyledText(
                    text:
                        '\n<bold>•	Jika arah arus pada kedua kawat searah maka pada kedua kawat akan tarik-menarik.</bold>'
                        '\n\n<bold>•	Sebaliknya jika arah arus pada kedua kawat berlawanan, maka kedua kawat akan tolak-menolak.</bold>',
                    tags: {
                      'bold': StyledTextTag(
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      'italic': StyledTextTag(
                          style: TextStyle(fontStyle: FontStyle.italic)),
                    },
                    textAlign: TextAlign.justify,
                    style: TextStyle(height: 1.10),
                  ),
                ),
                Text(
                  '\nGaya tarik-menarik atau gaya tolak-menolak pada kedua kawat merupakan akibat adanya gaya magnet pada kedua kawat tersebut. ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/2 searah.gif',
                  height: 300,
                  width: double.infinity,
                ),
                SizedBox(height: 10,),
                Image.asset(
                  'assets/2 berlawan.gif',
                  height: 300,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 13. Dua buah kawat lurus sejajar dengan arus listrik searah dan berlawanan arah.',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nBesarnya gaya magnet pada masing-masing kawat dapat dinyatakan : ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus12.png',
                  height: 50,
                  width: 250,
                ),
                Text(
                  '\nKarena arah B dan I saling tegak lurus atau T =90o maka nilai sin θ = 1, sehingga ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus13.png',
                  height: 50,
                  width: 250,
                ),
                Text(
                  '\nPada sub-bab mengenai hukum Biot-Savart, kita telah menghitung bahwa besarnya induksi magnet di sekitar kawat lurus berarus listrik adalah',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus14.png',
                  height: 50,
                  width: 300,
                ),
                Text(
                  '\nNilai B kemudian disubstitusikan ke dalam nilai F, sehingga kita akan mendapatkan',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus15.png',
                  height: 50,
                  width: 300,
                ),
                Text(
                  '\nJadi, kita bisa mengetahui besarnya gaya magnetik di antara kedua kawat sejajar yang dinyatakan sebagai gaya per satuan panjang yaitu :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus16.png',
                  height: 50,
                  width: 300,
                ),
                Image.asset(
                  'assets/dgn4.png',
                  height: 120,
                  width: double.infinity,
                ),
                Image.asset(
                  'assets/cthsoal3.png',
                  height: 240,
                  width: double.infinity,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
