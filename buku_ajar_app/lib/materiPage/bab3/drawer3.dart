import 'package:buku_ajar_app/homeScreen.dart';
import 'package:buku_ajar_app/materiPage/bab3/bab3.dart';
import 'package:buku_ajar_app/materiPage/bab3/bab3_2.dart';
import 'package:buku_ajar_app/materiPage/bab3/bab3_3.dart';
import 'package:buku_ajar_app/materiPage/bab3/bab3_lkpd.dart';
import 'package:buku_ajar_app/materiPage/bab3/bab3_quiz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class DrawerScreen3 extends StatefulWidget {
  @override
  _DrawerScreen3State createState() => _DrawerScreen3State();
}

class _DrawerScreen3State extends State<DrawerScreen3> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                image: AssetImage('assets/draw.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Text(''),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (c) => Home()),
                  (route) => false);
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "3.1 Gaya Magnetik pada Penghantar Berarus dalam Medan Magnet",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab3()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "3.2 Gaya Magnetik di Antara Dua Kawat Sejajar Berarus",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab3_2()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "3.3 Gaya Magnetik pada Muatan yang Bergerak dalam Medan Magnet",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab3_3()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.app_registration_outlined,
            title: "Ayo Mencoba!",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab3_quiz()),
              );
            },
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.app_registration_outlined,
              title: "Soal Latihan",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab3_lkpd()),
                );
              },
            ),
          ),
          Divider(color: Colors.white,),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
