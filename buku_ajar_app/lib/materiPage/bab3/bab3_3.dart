import 'package:buku_ajar_app/materiPage/bab3/drawer3.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class bab3_3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen3(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 50),
            Text(
              "Gaya Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet2.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only(right: 10, bottom: 10),
                  child: Text(
                    '3.3 Gaya Magnetik pada Muatan yang Bergerak '
                    '\n       dalam Medan Magnet',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Coba kalian ingat kembali materi tentang elektrostatik sebelum materi ini. Pada elektrostatik, kita tahu bahwa kuat arus adalah banyaknya muatan yang bergerak per satuan waktu (I = q/t). Nah, jika muatan tersebut bergerak dengan kecepatan <italic>v</italic>, kita dapat mengubah persamaan gaya Lorentz untuk muatan yang bergerak dalam medan magnet adalah :',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus17.png',
                  height: 45,
                  width: 250,
                ),
                Text(
                  '\nKarena lintasan yang ditempuh muatan dalam suatu selang waktu sana dengan besar kecepatan (v = ℓ/t) sehingga kita bisa mendapatkan :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus18.png',
                  height: 50,
                  width: 250,
                ),
                Text(
                  '\nDengan v = laju muatan (m/s) dan θ = sudut apit antara kecepatan v dengan medan magnet B.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nUntuk mengetahui arah dari gaya F, medan magnet B, dan kecepatan v, kita dapat menggunakan kaidah tangan kanan yang mirip dengan Gaya Lorentz, hanya saja yang membedakan hanyalah variabel I yang diganti dengan v. Perhatikan gambar 14.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    'assets/tangan kanan buka v.gif',
                    height: 300,
                    width: double.infinity,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 14. Penentuan arah Gaya Lorentz, medan magnet, dan kecepatan muatan yang bergerak dalam suatu medan magnet. ',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nApabila benda bermuatan listrik memasuki medan magnet dengan arah tegak lurus medan magnet (sin 90 = 1), maka benda bermuatan listrik tersebut akan bergerak dalam medan dengan lintasan yang berbentuk lingkaran. Hal tersebut dikarenakan gaya magnetik yang timbul akan berfungsi sebagai gaya sentripetal (Fs). Besarnya jari-jari lintasan yang ditempuh oleh muatan listrik dapat dihitung sebagai berikut :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset(
                  'assets/rumus19.png',
                  height: 40,
                  width: 270,
                ),
                Text(
                  '\nSehingga kita bisa mengetahui jari-jari lintasannya',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 10,),
                Image.asset(
                  'assets/rumus20.png',
                  height: 100,
                  width: 250,
                ),
                Image.asset(
                  'assets/dgn5.png',
                  height: 100,
                  width: double.infinity,
                ),
                Image.asset(
                  'assets/ctt.png',
                  height: 100,
                  width: double.infinity,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
