import 'package:float_column/float_column.dart';
import 'package:flutter/material.dart';

class Bab1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 25),
            Text(
              "Fenomena Magnetisme",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet1.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Image.asset(
                  'assets/magnet dasar.gif',
                  height: 200,
                  width: 200,
                ),
                Text(
                  'Sejarah kemagnetan dimulai ketika suatu peradaban kuno menemukan batu-batu (Fe3O4) yang memiliki sifat tarik-menarik. Peradaban kuno tersebut terletak di suatu daerah yang dikenal dengan nama Magnesia. Kemudian batu-batu '
                  'tersebut diberi nama magnet sesuai dengan tempat ditemukannya. Hingga saat ini, Magnet dikenal sebagai suatu benda yang memiliki kemampuan menarik benda–benda lain yang ada di sekitarnya karena di dalamnya terdapat suatu medan magnet.',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                FloatColumn(
                  children: [
                    Floatable(
                      float: FCFloat.end,
                      maxWidthPercentage: 0.547,
                      padding: const EdgeInsetsDirectional.only(start: 15),
                      child: Image(
                        image: AssetImage('assets/taukah.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    const WrappableText(
                        textAlign: TextAlign.justify,
                        text: TextSpan(
                            style: TextStyle(fontSize: 15.5),
                            text:
                                '\nTimbulnya fenomena magnetisme pada sebuah paku atau po- tongan besi yang tertarik oleh batang besi ialah salah satu contoh dari adanya sifat kemagnetan. Terdapat beberapa sifat kemagnetan lainnya, antara lain :')),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: Text(
                    '\n1.	Tidak semua benda dapat ditarik oleh magnet, sehingga magnet hanya bisa menarik benda–benda tertentu yang ada di sekitarnya.'
                    '\n\n2.	Magnet memiliki gaya magnet yang sifatnya dapat menembus benda, yang apabila gaya magnet ini besar maka gaya magnet dapat menembus benda yang tebal.'
                    '\n\n3.	Apabila ada dua magnet yang memiliki kutub berbeda, dan saling didekatkan maka mereka akan saling tarik menarik.'
                    '\n\n4.	Apabila kutub yang sejenis saling didekatkan satu sama lain maka mereka akan terjadi tolak-menolak'
                    '\n\n5.	Medan magnet akan membentuk gaya magnet, yang apabila sebuah benda didekatkan dengan magnet maka gaya magnet yang ditimbulkan magnetnya akan semakin besar dan sebaliknya.'
                    '\n\n6.	Jika suatu magnet terus menerus jatuh dan terbakar, maka Sifat kemagnetan dapat berkurang dan bahkan hilang.',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        height: 1.20),
                    textAlign: TextAlign.justify,
                  ),
                ),
                // Image.asset(
                //   'assets/taukah.png',
                //   height: 200,
                //   width: 200,
                // ),
                Text(
                  '\nBesaran yang menunjukkan kuatnya medan magnet yang ditimbulkan oleh suatu benda disimbolkan dengan huruf B dan memiliki satuan T (Tesla), dimana medan magnet (B) tersebut merupakan besaran vektor (memiliki nilai dan arah). ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nSuatu bahan yang didekatkan dengan magnet akan memiliki respon yang berbeda-beda. Hal ini dikarenakan atom penyusun bahan tersebut juga berbeda. Ada bahan yang dapat ditarik oleh magnet dengan sangat kuat, ada pula yang lemah, dan bahkan ada yang ditolak. Oleh karena itu kita dapat mengelompokkannya menjadi tiga jenis, yaitu bahan feromagnetik, paramagnetik, dan diamagnetik. ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      text:
                          '\n 1.	Bahan Feromagnetik                                                     ',
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                FloatColumn(
                  children: [
                    Floatable(
                      float: FCFloat.end,
                      maxWidthPercentage: 0.52,
                      padding: const EdgeInsetsDirectional.only(start: 23, top: 10),
                      child: Image(
                        image: AssetImage('assets/taukah2.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    const WrappableText(
                        textAlign: TextAlign.justify,
                        text: TextSpan(
                            style: TextStyle(fontSize: 16),
                            text:
                                '\nBahan feromagnetik dapat menimbulkan induksi yang besar dan sangat mudah dipengaruhi medan magnet. Akibatnya bahan ini akan sangat mudah jika ditarik oleh magnet karena banyaknya garis-garis medan magnetik luarnya. Contoh dari bahan feromagnetik adalah besi, nikel, kobalt, dan baja.')),
                  ],
                ),
                RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      text:
                          ' 2.	Bahan Paramagnetik                                                     ',
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                Text(
                  'Bahan paramagnetik merupakan bahan yang berada dalam medan magnetik yang cukup kuat tetapi ditarik dengan gaya yang sangat lemah. Bahan ini dapat menimbulkan induksi yang besar pada suatu medan magnet, tetapi induksinya lebih kecil daripada bahan feromagnetik. Contoh dari bahan paramagnetik adalah kayu, aluminium, dan platina.',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      text:
                          '\n 3.	Bahan Diamagnetik                                                     ',
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                Text(
                  'Bahan diamagnetik bersifat melawan ke- magnetan dari luar sehingga sulit dipengaruhi medan magnet luar. Bahan diamagnetik akan menimbulkan induksi magnet yang kecil jika bahan diamagnetik dimasukkan ke dalam medan magnet ini diberi medan magnet. Contoh dari bahan diamagnetik adalah emas, seng, bismuth, dan tembaga.',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
