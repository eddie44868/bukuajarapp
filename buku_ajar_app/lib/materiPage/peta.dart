import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class petaKonsep extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(width: 45),
              Text(
                "Peta Konsep",
                textAlign: TextAlign.center,
              ),
              SizedBox(width: 10),
              Image.asset(
                'assets/file.png',
                height: 25,
                width: 25,
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/petaKonsep.jpg',
                    height: 300,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/kd.png',
                    height: 250,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/tujuan.png',
                    height: 180,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/apersepsi.png',
                    height: 203,
                    width: double.infinity,
                  ),
                ),
                SizedBox(height: 15,)
              ]),
        )));
  }
}
