import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';

class bab2_22 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 0, bottom: 15),
                  child: Text(
                    '\n2.2.2 Medan Magnetik di sekitar kawat Melingkar '
                    '\n          Berarus Listrik',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                Image.asset(
                  'assets/gambar8.png',
                  height: 200,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 10,),
                  child: Text(
                    'Gambar 8. Sebuah kawat melingkar berarus. ',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nSelain disebabkan oleh kawat lurus berarus, medan magnet juga dapat timbul di sekitar kawat melingkar berarus, perhatikan Gambar 8. Titik P yang berjarak x dari kawat melingkar akan timbul medan magnet yang disebabkan oleh kawat melingkar, untuk menentukan besar medan magnet di titik P kita dapat menggunakan hukum Biot–Savart sehingga didapatkan besarnya medan magnet di titik P menggunakan persamaan :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 10,),
                Image.asset(
                  'assets/rumus5.png',
                  height: 80,
                  width: 200,
                ),
                Padding(
                  padding: const EdgeInsets.only( bottom: 10),
                  child: Text(
                    '\nApabila penghantar melingkar tersebut terdiri dari N lilitan, maka induksi magnetik di pusat lingkaran adalah :',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Image.asset(
                  'assets/rumus6.png',
                  height: 80,
                  width: 200,
                ),
                Text(
                  '\nJika titik induksinya berada di luar lingkaran : ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                 SizedBox(height: 10,),
                Image.asset(
                  'assets/rumus7.png',
                  height: 60,
                  width: 200
                ),
                Text(
                  '\nDengan N = banyaknya/jumlah lilitan kawat dan a = jari-jari lingkaran.'
                  '\n\nUntuk mengetahui arah medan magnet, kita masih menggunakan kaidah tangan kanan seperti pada Gambar 5 sebelumnya.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/cthsoal2.png',
                  height: 240,
                  width: double.infinity,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
