import 'package:buku_ajar_app/homeScreen.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_2.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_21.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_22.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_23.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_24.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_lkpd.dart';
import 'package:buku_ajar_app/materiPage/bab2/bab2_quiz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class DrawerScreen2 extends StatefulWidget {
  @override
  _DrawerScreen2State createState() => _DrawerScreen2State();
}

class _DrawerScreen2State extends State<DrawerScreen2> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                image: AssetImage('assets/draw.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Text(''),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (c) => Home()),
                  (route) => false);
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "2.1 Medan Magnetik di Sekitar Magnet",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab2()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "2.2 Medan Magnetik di Sekitar Kawat Berarus Listrik",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Bab2_2()),
              );
            },
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.bookmark_border,
              title: "2.2.1 Medan Magnetik di sekitar Kawat Lurus Panjang Berarus Listrik",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab2_21()),
                );
              },
            ),
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.bookmark_border,
              title: "2.2.2 Medan Magnetik di sekitar kawat Melingkar Berarus Listrik",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab2_22()),
                );
              },
            ),
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.bookmark_border,
              title: "2.2.3 Medan Magnetik di Pusat dan Ujung Solenoida",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab2_23()),
                );
              },
            ),
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.bookmark_border,
              title: "2.2.4 Medan Magnetik di Pusat Toroida",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab2_24()),
                );
              },
            ),
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.app_registration_outlined,
            title: "Ayo Mencoba!",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Bab2_quiz()),
              );
            },
          ),
          Divider(color: Colors.white,),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: DrawerListTile(
              iconData: Icons.app_registration_outlined,
              title: "Soal Latihan",
              onTilePressed: () {
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => bab2_lkpd()),
                );
              },
            ),
          ),
                    Divider(color: Colors.white,),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
