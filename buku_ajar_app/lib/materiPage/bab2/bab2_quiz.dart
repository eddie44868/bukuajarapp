import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Bab2_quiz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz1.png',
                    height: 400,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz2.png',
                    height: 200,
                    width: double.infinity,
                  ),
                ),
                RichText(
                  textAlign: TextAlign.start,
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: 'https://www.youtube.com/watch?v=iOck_OL7UGs',
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch(
                                'https://www.youtube.com/watch?v=iOck_OL7UGs');
                          },
                      ),
                      new TextSpan(
                        text: '.   ',
                        style: new TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz22.png',
                    height: 300,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz3.png',
                    height: 380,
                    width: double.infinity,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    right: 285,
                  ),
                  child: Text(
                    'Link 1 :',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SelectableText(
                  'https://phet.colorado.edu/translation/349/simulation/legacy/generator',
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 285),
                  child: Text(
                    'Link 2 :',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SelectableText(
                    'https://phet.colorado.edu/sims/faraday/generator_in.jar'),
                SizedBox(
                  height: 10,
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz4.png',
                    height: 450,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz5.png',
                    height: 470,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz6.png',
                    height: 430,
                    width: double.infinity,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
