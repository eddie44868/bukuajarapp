import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';

class bab2_21 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 30, bottom: 15),
                  child: Text(
                    '\n2.2.1 Medan Magnetik di sekitar Kawat Lurus '
                    '\n          Panjang Berarus Listrik',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                Image.asset(
                  'assets/gambar7.png',
                  height: 100,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 10, bottom: 10),
                  child: Text(
                    'Gambar 7. Sebuah kawat lurus panjang berarus. Pada titik P, induksi magnet akan masuk bidang',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nBerdasarkan hukum Biot-Savart, jika sebuah kawat lurus panjang yang dialiri arus sebesar I akan menimbulkan induksi magnet sebesar B. Secara matematis dituliskan :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 10,),
                Image.asset(
                  'assets/rumus4.png',
                  height: 100,
                  width: 200,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 270, bottom: 10),
                  child: Text(
                    '\nDengan :',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Image.asset(
                  'assets/dgn2.png',
                  width: double.infinity,
                ),
                Text(
                  '\nKarena B merupakan besaran vektor, sehingga memiliki arah yang dapat ditentukan dengan menggunakan kaidah tangan kanan seperti pada Gambar 5 sebelumnya. Jika titik P di sebelah kanan kawat dan arus listrik pada kawat penghantar dari bawah ke atas, maka arah medan magnet di titik P masuk bidang gambar. Jika untuk P di sebelah kiri, arah medan magnetnya keluar bidang gambar.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                 SizedBox(height: 10,),
                Image.asset(
                  'assets/cthsoal.png',
                  height: 400,
                  width: double.infinity,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
