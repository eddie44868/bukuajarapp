import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class bab2_23 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 15, right: 40),
                  child: Text(
                    '\n2.2.3 Medan Magnetik di Pusat dan Ujung '
                    '\nSolenoida',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Solenoida merupakan sebuah kawat panjang yang dililitkan di dalam sebuah <italic>helix</italic> yang terbungkus rapat. Kita menganggap bahwa <italic>helix</italic> tersebut sangat panjang dibandingkan diameternya. Jarak antara lilitan biasanya kecil (rapat) dan lilitannya dapat terdiri atas satu lapisan atau lebih seperti Gambar 9.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset(
                  'assets/solenoida.gif',
                  height: 330,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 10, bottom: 10),
                  child: Text(
                    'Gambar 9. Medan magnet pada Solenoida',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  'Besarnya induksi magnetik di pusat atau ujung solenoida dirumuskan sebagai berikut :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text(
                          'Pusat Solenoida',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              height: 1.25),
                          textAlign: TextAlign.justify,
                        ),
                        Image.asset(
                          'assets/rumus8.png',
                          height: 80,
                          width: 100,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          'Ujung Solenoida',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              height: 1.25),
                          textAlign: TextAlign.justify,
                        ),
                        Image.asset(
                          'assets/rumus9.png',
                          height: 80,
                          width: 100,
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    'Dengan : N = jumlah lilitan dan l = panjang solenoida (m).',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
