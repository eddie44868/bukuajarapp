import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';

class bab2_24 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 15, right: 60),
                  child: Text(
                    '\n2.2.4 Medan Magnetik di Pusat Toroida',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                Text(
                  'Jika sebuah solenoida dilengkungkan sehingga sumbunya membentuk lingkaran dinamakan Toroida seperti pada Gambar 10.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 200, bottom: 10),
                  child: Text(
                    'Gambar 10. Medan magnet pada Toroida',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  'Besarnya induksi magnetik di pusat toroida dirumuskan sebagai berikut :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus10.png',
                  height: 80,
                  width: 100,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 70),
                  child: Text(
                    'Dengan : R = jari-jari efektif toroida (m). ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          Image.asset(
            'assets/toroida.gif',
            height: 330,
            width: double.infinity,
          ),
        ],
      ),
    );
  }
}
