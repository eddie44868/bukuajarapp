import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class Bab2_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 80, bottom: 15),
                  child: Text(
                    '\n2.2 Medan Magnetik di Sekitar Kawat'
                    '\n       Berarus Listrik',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                Image.asset(
                  'assets/org.png',
                  height: 200,
                  width: 300,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 10, bottom: 10),
                  child: Text(
                    'Gambar 4. Hans Christian Oersted (1777-1851), fisikawan berkebangsaan Denmark',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                StyledText(
                  text:
                      '<bold>Hans Christian Oersted</bold> setelah melakukan eksperimen cukup lama, pada tahun 1819 ia berhasil menemukan bahwa, <italic>”Jika sebuah magnet jarum (kompas kecil) didekatkan pada suatu penghantar yang berarus listrik, magnet jarum akan menyimpang”.</italic> Eksperimen ini menunjukkan bahwa di sekitar kawat berarus terdapat medan magnet.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nArah medan magnet (B) yang disebabkan oleh kawat berarus listrik dapat ditentukan dengan menggunakan kaidah tangan kanan seperti pada Gambar 5. Arah ibu jari menunjukkan arah arus listrik, sedangkan putaran keempat jari lainnya menunjukkan arah medan magnet',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/catatan.png',
                  height: 120,
                  width: 400,
                ),
                Image.asset(
                  'assets/kanan.gif',
                  height: 330,
                  width: double.infinity,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Text(
                      'Gambar 5. Kaidah tangan kanan untuk menentukan arah medan dan kuat arus pada kawat berarus listrik',
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          height: 1.25),
                      textAlign: TextAlign.justify,
                    )),
                Image.asset(
                  'assets/cth.png',
                  height: 310,
                  width: 310,
                ),
                StyledText(
                  text:
                      'Pada saat Oersted mengadakan percobaan untuk mengamati hubungan antara kelistrikan dan kemagnetan, ia belum sampai menghitung besarnya kuat medan magnet di suatu titik di sekitar kawat berarus. Perhitungan secara matematik baru dikemukakan oleh ilmuwan dari Prancis yaitu <bold>Jean Bastiste Biot</bold> dan <bold>Felix Savart</bold>. Berdasarkan hasil eksperimennya tentang pengamatan medan magnet di suatu titik P yang dipengaruhi oleh suatu kawat penghantar dℓ, yang dialiri arus listrik I diperoleh kesimpulan bahwa besarnya kuat medan magnet (yang kemudian disebut induksi magnet yang diberi lambang B) di titik P : ',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\na.	Berbanding lurus dengan kuat arus listrik (ℓ).'
                  '\nb.	Berbanding lurus dengan panjang kawat (dℓ).'
                  '\nc.	Berbanding terbalik dengan kuadrat jarak antara '
                  '\n    titik P ke elemen kawat penghantar (r).'
                  '\nd.	Sebanding dengan sin θ, yaitu sinus sudut apit  '
                  '\n    T antara arah arus dengan garis hubung antara'
                  '\n    titik P ke elemen kawat penghantar.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.30),
                  textAlign: TextAlign.left,
                ),
                Image.asset(
                  'assets/6.png',
                  height: 250,
                  width: 250,
                ),
                Padding(
                    padding:
                        const EdgeInsets.only(left: 30, right: 30, bottom: 10),
                    child: Text(
                      'Gambar 6. Induksi magnetik dB akibat elemen penghantar dℓ berarus listrik ',
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          height: 1.25),
                      textAlign: TextAlign.justify,
                    )),
                StyledText(
                  text:
                      'Pernyataan di atas dikenal dengan hukum <bold>Biot-Savart</bold> yang secara matematis dinyatakan dalam persamaan :',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/rumus1.png',
                  height: 100,
                  width: 250,
                ),
                Text(
                  'Dengan k = konstanta (Wb/Am) yang memenuhi hubungan :',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.30),
                  textAlign: TextAlign.left,
                ),
                Image.asset(
                  'assets/rumus2.png',
                  height: 100,
                  width: 250,
                ),
                Text(
                  'Sehingga persamaan 2.1 dapat ditulis menjadi',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.30),
                  textAlign: TextAlign.left,
                ),
                Image.asset(
                  'assets/rumus3.png',
                  height: 100,
                  width: 250,
                ),
                Image.asset(
                  'assets/dgn.png',
                  height: 100,
                  width: double.infinity,
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
