import 'package:buku_ajar_app/materiPage/bab2/drawer2.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

class bab2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen2(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Medan Magnetik",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/magnet3.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Text(
                  'Timbulnya medan magnet pada sebuah benda disebabkan karena berbagai macam faktor, yaitu medan magnetik di sekitar magnet dan medan magnet di sekitar kawat berarus listrik.',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 70),
                  child: Text(
                    '\n2.1 Medan Magnetik di Sekitar Magnet',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.20),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Medan magnetik adalah <italic>ruang di sekitar magnet dimana magnet lain atau benda lain yang dapat dipengaruhi magnet akan mengalami gaya magnetik.</italic> Sehingga jika kita meletakkan kertas di atas sebuah magnet, dan kemudian kita menaburkan serbuk besi di atas permukaan kertas tersebut, akan terlihat garis-garis gaya magnet di sekitar magnet tersebut seperti pada Gambar 2.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/garis.png',
                  height: 200,
                  width: 300,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 2. Pola garis gaya magnet yang dibentuk oleh sebuah magnet batang',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                StyledText(
                  text:
                      '\nBerdasarkan Gambar 2, terlihat bahwa serbuk besi paling banyak menempel pada ujung-ujung magnet. Hal ini dikarenakan ujung-ujung magnet memiliki gaya magnetik yang paling kuat dibanding pada bagian tengah magnet tesebut. Ujung-ujung magnet biasanya kita sebut dengan <italic>kutub magnet</italic> yang terdiri atas <italic>kutub utara</italic> (U) dan <italic>kutub selatan</italic> (S). Sedangkan, garis lurus yang menghubungkan kedua kutub ini disebut dengan sumbu magnet.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nGaris-garis gaya magnet mematuhi hukum selalu keluar dari kutub utara, dan masuk ke kutub selatan. Sementara di dalam sumbu magnet, garis-garis gaya magnet memiliki arah dari selatan ke utara. Garis-garis tersebut tidak pernah saling berpotongan dan kerapatan garis-garis tersebut menunjukkan kekuatan medan magnetik.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Text(
                  '\nOleh karena itu, jika terdapat dua kutub tidak sejenis saling berhadapan akan diperoleh garis-garis gaya magnet yang berhubungan, akibatnya magnet akan saling tarik menarik. Sebaliknya, jika terdapat dua kutub yang sejenis berhadapan, akan diperoleh garis gaya magnet yang menekan saling menjauhi, akibatnya magnet akan saling tolak-menolak. Perhatikan Gambar 3.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Image.asset(
                          'assets/tarik menarik.gif',
                          height: 150,
                          width: 150,
                        ),
                        Text(
                          '(a)',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Image.asset(
                          'assets/tolak menolak.gif',
                          height: 150,
                          width: 150,
                        ),
                        Text(
                          '(b)',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Gambar 3. Pola garis-garis gaya magnet yang terbentuk untuk dua buah magnet batang yang didekatkan (a) ketika kutubnya sama dan (b) ketika kutubnya berbeda.',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
