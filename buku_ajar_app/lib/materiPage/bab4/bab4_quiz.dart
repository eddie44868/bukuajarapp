import 'package:buku_ajar_app/materiPage/bab4/drawer4.dart';
import 'package:flutter/material.dart';

class bab4_quiz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen4(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Penerapan Magnetisme dalam'
              '\nProduk Teknologi dan Kehidupan Sehari-hari',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 13),
            ),
            SizedBox(width: 5),
            Image.asset(
              'assets/house.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz8.png',
                    height: 420,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz9.png',
                    height: 485,
                    width: double.infinity,
                  ),
                ),
                InteractiveViewer(
                  minScale: 0.3,
                  maxScale: 5,
                  alignPanAxis: true,
                  child: Image.asset(
                    'assets/quiz10.png',
                    height: 530,
                    width: double.infinity,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
