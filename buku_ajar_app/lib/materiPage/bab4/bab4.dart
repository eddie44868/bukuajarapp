import 'package:buku_ajar_app/materiPage/bab4/drawer4.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';
import 'package:url_launcher/url_launcher.dart';

class bab4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.blue,
        ),
        child: DrawerScreen4(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Penerapan Magnetisme dalam'
              '\nProduk Teknologi dan Kehidupan Sehari-hari',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 13),
            ),
            SizedBox(width: 5),
            Image.asset(
              'assets/house.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Text(
                  'Penerapan magnetisme, khususnya gaya magnetik, biasanya banyak dijumpai pada teknologi yang memiliki prinsip kerja mengubah energi listrik menjadi energi gerak. Contohnya adalah meteran listrik. Selain itu, medan magnet juga bisa dijumpai pada transportasi dan peralatan elektronik.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.20),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 200, bottom: 10),
                  child: Text(
                    '\n4.1 Meteran Listrik ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Pada dasarnya, meteran listrik terdiri atas sebuah loop atau kumparan kawat yang diletakkan dalam medan magnetik suatu magnet permanen. Ketika arus mengalir melalui loop, medan magnetik mengerjakan gaya magnetik pada kawat yang mendatar. Gaya pada kawat yang terletak di sebelah kiri dan sebelah kanan berlawanan arah. Kedua gaya ini membentuk pasangan gaya yang dapat memutar loop pada sumbu vertikal. Loop tersebut dihubungkan dengan jarum penunujuk.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Image.asset(
                            'assets/gambar15_1.png',
                            height: 150,
                            width: 150,
                          ),
                          Text(
                            '(a)',
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          Image.asset(
                            'assets/gambar15_2.png',
                            height: 150,
                            width: 150,
                          ),
                          Text(
                            '(b)',
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Text(
                    'Gambar 15. (a) Meteran listrik yang umum kita jumpai (b) Skema komponen-komponen magnetisme yang memengaruhi prinsip kerja meteran listrik.',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nOleh karena gaya pada kawat berarus sebanding dengan kuat arus yang mengalir, maka simpangan yang ditunjukkan oleh jarum penunjuk ini dapat dianggap sebagai ukuran besar arus listrik yang mengalir pada loop. Alat-alat lain yang menggunakan prinsip ini diantaranya adalah pengeras suara, neraca arus, elekrodinamometer, dan sinklotron.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 200, bottom: 10),
                  child: Text(
                    '\n4.2 Kereta Maglev ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Maglev merupakan singkatan dari <italic>Magnetic Levitation</italic> yang berarti pengangkatan magnetik. Hal ini berarti roda dari kereta maglev tidak bersentuhan dengan relnya, tetapi mengambang sekitar 10 mm diatasnya dan didorong oleh magnet bermuatan listrik. Badan kereta yang mengambang tersebut, membuatnya tidak mengalami gaya gesek antara badan kereta dan rel ataupun badan kereta dengan dinding beton. Sehingga satu-satunya yang memperlambat kereta maglev adalah gaya gesek dengan udara.'
                      '\n\nHal ini dapat dikurangi dengan membuat bentuk kereta yang aerodinamis sehingga dapat mengurangi gaya gesek dengan udara. Kecilnya gaya gesek serta desain aerodinamis inilah yang membuat kereta maglev dapat berjalan dengan sangat cepat mencapai 600 kilometer/jam dan dinobatkan sebagai kendaraan tercepat di dunia. Lalu, bagaimana prinsip kerja kereta ini? Cari tahu melalui lembar kerja peserta didik setelah materi ini atau kamu bisa melihat video berikut ini :',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                RichText(
                  textAlign: TextAlign.start,
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: 'https://www.youtube.com/watch?v=AG4b08guRys',
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch(
                                'https://www.youtube.com/watch?v=AG4b08guRys');
                          },
                      ),
                      new TextSpan(
                        text: '!   ',
                        style: new TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                ),
                Image.asset(
                  'assets/gambar16.png',
                  height: 200,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 5),
                  child: Text(
                    'Gambar 16. Kereta maglev yang ada di Cina',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 250, bottom: 10),
                  child: Text(
                    '\n4.3 Kompas ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Siapa yang tidak mengenal kompas? Kompas adalah alat penunjuk arah mata angin. Alat ini menggunakan medan magnet bumi untuk menentukan arah utara, selatan, timur dan barat. Kompas magnetik pertama kali ditemukan dan digunakan sebagai alat untuk meramal pada tahun 200 SM atau awal Dinasti Han Tiongkok. Dan hingga saat ini kompas telah berkembang pesat hingga muncul kompas digital yang dapat kalian gunakan di ponsel kalian masing-masing.'
                      '\n\nPrinsip kerja kompas adalah adanya gaya tarik menarik antara magnet pada jarum kompas dengan kutub magnet bumi. Jarum kompas yang terbuat dari magnet memiliki kutub utara dan selatan dan akan selalu menunjuk arah utara dan selatan. Medan magnet bumi memberikan gaya magnet kepada jarum kompas. Kutub utara jarum kompas menunjuk ke arah kutub utara geografis bumi.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Image.asset(
                            'assets/gambar17_1.png',
                            height: 100,
                            width: 150,
                          ),
                          Text(
                            '(a)',
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          Image.asset(
                            'assets/gambar17_2.png',
                            height: 100,
                            width: 150,
                          ),
                          Text(
                            '(b)',
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Text(
                    'Gambar 17. (a) Kompas analog (b) Medan magnet dan garis-garis gaya magnet bumi',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\nSeperti yang kita ketahui, kutub magnet yang senama tolak-menolak. Jadi, yang ditunjuk oleh kutub utara jarum kompas sebenarnya adalah kutub selatan magnet bumi. Sedangkan yang ditunjuk oleh kutub selatan jarum kompas sebenarnya kutub utara magnet bumi.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 240, bottom: 10),
                  child: Text(
                    '\n4.4 Mikrofon ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Microphone adalah suatu komponen elektronika yang dapat mengubah gelombang suara menjadi energi listrik. Jenis-jenis microphone memiliki cara yang berbeda dalam mengubah bentuk energinya, namun memiliki satu bagian utama yang sama yaitu diafragma.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/gambar18.png',
                  height: 150,
                  width: 200,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 10, bottom: 10),
                  child: Text(
                    'Gambar 18. Komponen magnetostatik yang memengaruhi mikrofon',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        height: 1.25),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  'Prinsip kerja microphone dimulai ketika gelombang suara yang menabrak diafragma yang terdiri dari membran plastik yang sangat tipis. Diafragma akan bergetar sesuai dengan gelombang suara yang diterimanya. Bergetarnya diafragma akan ikut menggetarkan kumparan (coil) yang terdapat di bagian belakang diafragma. Kemudian magnet permanen yang dikelilingi oleh kumparan akan menimbulkan medan magnet disekitarnya yang akan menimbulkan sinyal listrik. Sinyal listrik yang dihasilkan tersebut kemudian akan mengalir ke amplifier.',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 210, bottom: 10),
                  child: Text(
                    '\n4.5 Motor Listrik ',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                StyledText(
                  text:
                      'Prinsip kerja motor listrik adalah dengan mengubah energi listrik menjadi energi mekanik. Dalam hal ini, kumparan diputar memotong garis-garis gaya medan magnet yang dihasilkan oleh kutub-kutub magnet permanen (stator). Sehingga akan timbul induksi elektromagnetik yang menyebabkan di ujung kumparan timbul Gaya Gerak Listrik Induksi. Alat ini banyak ditemui pada kipas angin, kompresor, alat pengering rambut, dan mainan anak.',
                  tags: {
                    'bold': StyledTextTag(
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    'italic': StyledTextTag(
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  },
                  textAlign: TextAlign.justify,
                ),
                Image.asset(
                  'assets/motor.jpg',
                  height: 250,
                  width: double.infinity,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
