import 'package:buku_ajar_app/homeScreen.dart';
import 'package:buku_ajar_app/materiPage/bab4/bab4.dart';
import 'package:buku_ajar_app/materiPage/bab4/bab4_quiz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class DrawerScreen4 extends StatefulWidget {
  @override
  _DrawerScreen4State createState() => _DrawerScreen4State();
}

class _DrawerScreen4State extends State<DrawerScreen4> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                image: AssetImage('assets/draw.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Text(''),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (c) => Home()),
                  (route) => false);
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Penerapan Magnetisme dalam Produk Teknologi dan Kehidupan Sehari-hari",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab4()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Ayo Mencoba!",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => bab4_quiz()),
              );
            },
          ),
          Divider(color: Colors.white,),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
