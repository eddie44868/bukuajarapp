import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            Text(
              "Tentang Aplikasi",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 10),
            Image.asset(
              'assets/info.png',
              height: 25,
              width: 25,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            //: size.height * .4,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/back2.png'),
              fit: BoxFit.fill,
            )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Text(
                    'BUKU AJAR DIGITAL'
                    '\nMEDAN MAGNET DAN PENERAPANNYA',
                    style: TextStyle(
                        fontSize: 19.5,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 55),
                  child: Text(
                    'Fisika SMA/MA Kelas XII Semester 1',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        height: 1.25),
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 80, 0),
                  child: Text(
                    '\nPenyusun		: Iqbal Ainur Rizki'
                    '\n                      Khoirun Nisa’'
                    '\n                      Nina Fajriyah Citra'
                    '\n                      Hanan Zaki Alhusni'
                    '\n\nPengarah		: Binar Kurnia Prahani',
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                        height: 1.1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 55, 0),
                  child: Text(
                    '\nDesain Sampul	: Hanan Zaki Alhusni'
                    '\n                              Iqbal Ainur Rizki',
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                        height: 1.1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Text(
                  '\n\nAplikasi ini dibuat dengan menggunakan ',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/flutter.png',
                      height: 50,
                      width: 200,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        '2.2.1 ',
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                            height: 1.25),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
                Text(
                  '©2021',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      height: 1.25),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
