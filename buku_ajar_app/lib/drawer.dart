import 'package:buku_ajar_app/homeScreen.dart';
import 'package:buku_ajar_app/materiPage/bab1/bab1.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                image: AssetImage('assets/draw.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Text(''),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (c) => Home()),
                  (route) => false);
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Materi 1",
            onTilePressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Bab1()),
              );
            },
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Materi 2",
            onTilePressed: () {},
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Materi 3",
            onTilePressed: () {},
          ),
          Divider(color: Colors.white,),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Materi 4",
            onTilePressed: () {},
          ),
          Divider(color: Colors.white,),

        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
